$(function () {
    // show random tab on page load
    var $activeTab = $('.state'); // this targets current 'active' tab
    var index = Math.floor($activeTab.length * Math.random()); // generates a random number which defines the 'active' tab
    $('.state:eq(' + index + ')').prop('checked', true).attr('aria-selected', true).attr('aria-checked', true); // adds 'checked' and 'aria-selected=true' to the opened tab

    // switch between tabs by setting 'aria-selected' as true
    $('.state').change(function () {
        $(this).parent().find('.state').each(function () {
            if (this.checked) {
                $(this).attr('aria-selected', 'true');
            } else {
                $(this).removeAttr('aria-selected');
            }
        });
    });

    // hide text under "mehr Informationen" on pageload
    $('[class^=tab-sub-content_]').hide();

    // toggle "mehr Informationen"
    $('[class^=more-info]').click(function (event) {
        event.preventDefault();
        $('.tab-sub-content_' + this.className).slideToggle('slow');

        if ($(this).text() == " Informationen ausblenden")
            $(this).text(" mehr Informationen")
        else
            $(this).text(" Informationen ausblenden")
    });
});