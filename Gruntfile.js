module.exports = function (grunt) {
    // Projektkonfiguration
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        uglify: {
            build: {
                src: 'src/js/main.js',
                dest: 'src/js/main.min.js'
            }
        },
        sass: {
            dist: {
                options: {
                    style: 'expanded'
                },
                files: {
                    'src/css/main.css': 'src/scss/main.scss'}
            }
        },
        watch: {
            css: {
                files: '**/*.scss',
                tasks: ['sass']
            },
            js: {
                files: 'src/js/*.min.js',
                tasks: ['uglify']
            },
            html: {
                files: 'src/*.html',
                tasks: ['copy']
            },
            jpg: {
                files: 'src/img/*.jpg',
                tasks: ['imagemin']
            }
        },
        cssmin: {
            target: {
                files: [
                    {
                        expand: true,
                        cwd: 'src/css',
                        src: ['*.css'],
                        dest: 'dist/assets/css',
                        ext: '.min.css'
                    }
                ]
            }
        },
        copy: {
            files: {
                cwd: 'src/',
                src: '**/*.html',
                dest: 'dist/',
                expand: true
            }
        },
        imagemin: {
            dynamic: {
                files: [{
                    expand: true,
                    cwd: 'src/',
                    src: ['**/*.{png,jpg,gif}'],
                    dest: 'dist/assets/'
                }]
            }
        }
    });

    require('load-grunt-tasks')(grunt);

    // DEV
    grunt.registerTask('default', ['imagemin','uglify','watch', 'cssmin']);
    // PROD
    grunt.registerTask('prod', ['cssmin']);
};